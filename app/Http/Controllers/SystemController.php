<?php

namespace App\Http\Controllers;

use App\SnowGoose;
use App\Http\Requests;
use Illuminate\Http\Request;

class SystemController extends Controller
{

	public function index() {
		
		$laravel_versions = json_decode(file_get_contents("https://packagist.org/p/laravel/laravel.json"))->packages->{"laravel/laravel"};

		return view()->make('snowgoose',compact('laravel_versions'));
		
	}
	
}
