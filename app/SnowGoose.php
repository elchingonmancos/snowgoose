<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SnowGoose extends Model
{
	

	private $root_path;
	
	private $update_path;
	
	private $archive_path;
	
	private $migration_errors = [];

	public $update_prefix = '_snowgoose';
	
	public $exclude = ['','.','..','*','.DS_Store','vendor','node_modules','storage'];
	
	public $composer_require_exclude = ['php','laravel/framework'];
	
	public $composer_require_dev_exclude = ['fzaninotto/faker','mockery/mockery','phpunit/phpunit','symfony/css-selector','symfony/dom-crawler'];
	
	public function __construct($root_path)
	{

		$this->root_path = $root_path;
		
		$this->update_path = $root_path . '' . $this->update_prefix;
		
		$this->archive_path = $root_path . "_" . date("j-n-Y") . "_snowgoose.archive";
		
		$exploded_path = explode('/',$this->root_path);
		
		# Exclude the path leading up to the project
		foreach($exploded_path as $segment) {
			
			if ($segment === end($exploded_path)) continue;
			
			$this->exclude[] = $segment;
			
		}
		
	}
	
	public function addMigrationError($type,$message,$source_path,$destination_path)
	{
		$this->migration_errors[] = [
			'type' => $type,
			'message' => $message,
			'source_path' => $source_path,
			'destination_path' => $destination_path
		];
	}
	
	
	public function makePathRelative($path)
	{

		return str_replace([$this->getProjectUpdatePath(),$this->getProjectRootPath()], "", $path);
		

	}
	
	public function pathSwap($path)
	{
		
		if (strstr($path,$this->update_path)) {
			
			return str_replace($this->update_path, $this->root_path, $path);
			
		} elseif (strstr($path,$this->root_path)) {
			
			return str_replace($this->root_path, $this->update_path, $path);
			
		} else {
			
			return false;
			
		}

	}


	public function getProjectTree($path,$exclude=[])
	{

		if (!file_exists($path)) {
			
			return false;
			
		}
		
		if (empty($exclude)) {
			
			$exclude = $this->exclude;
			
		}
		
		$Directory = new \RecursiveDirectoryIterator($path,\RecursiveDirectoryIterator::SKIP_DOTS);
		
		$Filter = new \RecursiveCallbackFilterIterator($Directory,
					
						function ($fileInfo, $key, $iterator) use ($exclude) {
							return $fileInfo->isFile() || !in_array($fileInfo->getBaseName(), $exclude);
						}
					);
		
		$Iterator = new \RecursiveIteratorIterator($Filter);

		$tree = [];
		
		foreach ($Iterator as $key => $file) { 
			

			
			if ($file->isDir()) {
				
				$path = [$file->getFilename() => []];
				
				
			} else {
				
				$path = [$this->makePathRelative($file->getPathname())]; 
				
			}
		
			for ($depth = $Iterator->getDepth() - 1; $depth >= 0; $depth--) {

				$path = [$this->makePathRelative($Iterator->getSubIterator($depth)->current()->getPathname()) => ['children' => $path]]; 

			} 

			$tree = array_merge_recursive($tree, $path); 
		} 
		
		return $tree;
		
	}
	
	########################
	#   SYSTEM COMMANDS    #
	########################
	
	public function migrateComposerConfig($root_composer_config_path,$update_composer_config_path)
	{
		
		if (!$root_composer_config_path || !$update_composer_config_path) {
			
			return false;
			
		}
		
		if (!$update_composer_config = json_decode(file_get_contents($update_composer_config_path),true)) {
			
			return false;
			
		}
		
		if (!$root_composer_config = json_decode(file_get_contents($root_composer_config_path),true)) {
			return false;
		}
		
		$update_composer_config['name'] = $root_composer_config['name'];
		$update_composer_config['description'] = $root_composer_config['description'];
		$update_composer_config['keywords'] = $root_composer_config['keywords'];
		$update_composer_config['type'] = $root_composer_config['type'];
		
		foreach($root_composer_config['require'] as $key => $item) {
			
			if (in_array($key, $this->composer_require_exclude)) {
				continue;
			}
			
			$update_composer_config['require'][$key] = $item;
			
		}
		
		foreach($root_composer_config['require-dev'] as $key => $item) {
			
			if (in_array($key, $this->composer_require_dev_exclude)) {
				
				continue;
			}
			
			$update_composer_config['require-dev'][$key] = $item;
			
		}
		
		$update_composer_config['autoload'] = $root_composer_config['autoload'];
		$update_composer_config['autoload-dev'] = $root_composer_config['autoload-dev'];
		$update_composer_config['scripts'] = $root_composer_config['scripts'];
		$update_composer_config['config'] = $root_composer_config['config'];
		
		if (array_key_exists('repositories', $root_composer_config)) {
			
			$update_composer_config['repositories'] = $root_composer_config['repositories'];
		
		}
		
		if (!$file = fopen($update_composer_config_path, 'w+')) {
			
			return false;
			
		}
		
		if (!fwrite($file,json_encode($update_composer_config,JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES))) {

			return false;
			
		}
		
		return true;
		
	}
	
	public function composerUpdate($path)
	{
		
		if (!file_exists($path)) {
			
			return false;
			
		}
		
		putenv('COMPOSER_HOME=/Users/jdimarco/.composer'); //Have to make this dynamic
		
		exec('/usr/local/bin/composer update -d ' . $path,$output,$status);
		
		// Status code keep returning '1', but everything runs fine...	
		#if ($status !== 0) {
		#	return false;
		#}
		
		return true;
	}
	
	
	
	public function composerInstall($path)
	{
		
		if (!file_exists($path)) {
			
			return false;
			
		}
		
		putenv('COMPOSER_HOME=/Users/jdimarco/.composer'); //Have to make this dynamic

		exec('/usr/local/bin/composer install -d ' . $path,$output,$status);
		
		// Status code keep returning '1', but everything runs fine...	
		#if ($status !== 0) {
		#	return false;
		#}
		
		return true;
	}
	
	
	
	public function composerDumpAutoload($path)
	{
		
		if (!file_exists($path)) {
			
			return false;
			
		}
		
		putenv('COMPOSER_HOME=/Users/jdimarco/.composer'); //Have to make this dynamic

		if (system('/usr/local/bin/composer dump-autoload -d ' . $path));
			
		return true; // Won't return bool, but a pretty basic command.
			
	}
	
	
	public function removeContents($target,$excluded=['.','..'])
	{
		
		if (!file_exists($target)) {
			
			return false;
			
		}
		
		if ((is_file($target)) && (!in_array(basename($target), $excluded)) && (!file_exists($target) || !unlink($target))) {

			return false;
			
		}
		
		if (is_dir($target)) {
			
			if (!in_array(basename($target), $excluded)) { // Move this to top of method
				
				$sub_targets = scandir($target);
				
				if (count($sub_targets) > 2) {
					
					foreach($sub_targets as $sub_target) {
						
						if (!in_array($sub_target, $excluded)) {
							
							$this->removeContents($target."/".$sub_target);
							
						}
						
					}
					
					if (!rmdir($target)) {
						
						return false;
						
					}
				}
			}
			
		}
	}
	
	
	
	public function downloadPackage($version)
	{

		putenv('COMPOSER_HOME=/Users/jdimarco/.composer'); // have to make this dynamic ... and throw an error if it cant be found.
		
		exec('/usr/local/bin/composer create-project laravel/laravel=' . $version . ' ' . $this->update_path . ' --prefer-dist 2>&1',$output,$return);
		
		if ($return == 0) return true; // 0 is a successful return code
		
		return false;
		
	}
	
	
	
	public function installPackage()
	{
		
		exec('/usr/local/bin/composer install -d ' . $this->update_path . ' 2>&1',$output,$status); // composer path needs to be dynamic
		
		if ($status == 0) return true;
		
		return false;
		
	}



	public function migrateENV()
	{
		if (!file_exists($this->root_path."/.env")) return false;
		
		if (!copy($this->root_path."/.env", $this->update_path."/.env")) return false;
		
		return true;
	}
	
	
	
	public function migrateFile($source_path,$destination_path,$options=[])
	{
		
		$status = true;
		
		$exploded_destination_path = explode('/',$destination_path);
		
		$target = "";
		
		$last_element = end($exploded_destination_path);
		
		foreach($exploded_destination_path as $segment) {
			
			$target .= "/" . $segment;
			
			if (!file_exists($target) && $segment !== $last_element) {
				
				mkdir($target);
				
			}
			
		}
			
		if (is_file($source_path) && is_dir($destination_path)) {
			
			copy($source_path, $destination_path.'/'.basename($source_path));
			
		} elseif (is_file($source_path) && !copy($source_path, $destination_path)) {

			return false;
			
		}
		
		if (is_dir($source_path) && !is_dir($destination_path)) {
			
			mkdir($destination_path);
			
			if (scandir($source_path) > 2) {
				
				$dir = new \RecursiveDirectoryIterator($source_path);
				
				foreach ($dir as $source) {
					
					$destination = $this->pathSwap($source);
					
					$this->migrateFile($source,$destination);
					
				}
				
				
				
			}
			
		}
		
		return $status;
	}
	
	public function migrateNodeModules()
	{
		
		if (!is_dir($this->update_path . '/node_modules')) mkdir($this->update_path . '/node_modules');
		
		$root_json_package = json_decode(file_get_contents($this->root_path.'/package.json'),true);
		$update_json_package = json_decode(file_get_contents($this->update_path.'/package.json'),true);
		
		foreach ($root_json_package as $key => $val) {
			
			if (is_array($val) && array_key_exists($key, $update_json_package)) {
				
				foreach ($val as $name => $version) {
					
					$update_json_package[$key][$name] = $version;
					
				}
				
			} elseif (!array_key_exists($key, $update_json_package)) {
				
				$update_json_package[$key] = $val;
				
			}
			
		}
		
		file_put_contents($this->update_path.'/package.json', json_encode($update_json_package).PHP_EOL);

		$current_path = exec('echo $PATH');
		putenv("PATH='/Applications/MAMP/bin/php/php7.0.10/bin:/usr/local/bin:" . $current_path . "'");
		$last_line = exec('cd ' . $this->update_path . ' && npm install',$data,$exit_code);
		
		return true;

	}
	
	public function migrateGit()
	{
		
		if ($this->migrateFile($this->root_path.'/.git',$this->update_path.'/.git')) {
			
			return true;
			
		}
		
		return false;
		
	}
	
	public function migrateGulp()
	{

		if (copy($this->root_path.'/gulpfile.js',$this->update_path.'/gulpfile.js')) {
			
			return true;
			
		}
		
		return false;
		
	}
	
	public function addExcludeItem($name)
	{
		array_push($this->exclude, $name);
	}
	
	
	########################
	#   PROPERTY GETTERS   #
	########################
	
	public function getProjectRootPath()
	{
		return $this->root_path;
	}
	
	public function getProjectUpdatePath()
	{
		return $this->update_path;
	}
	
	public function getProjectArchivePath()
	{
		return $this->archive_path;
	}
	
	public function getMigrationErrors()
	{
		return $this->migration_errors;
	}
	
	private function getComposerPath()
	{
		return system("which composer");
	}
	
	private function getUserHomePath()
	{
		return system('eval echo ~$USER');
	}
	
	public function getProjectDependencies($path)
	{
		
		if (!file_exists($path)) {
			
			return false;
			
		}
		
		if (!$dependencies = json_decode(file_get_contents($path . "/composer.json"),true)) {
			
			return false;
			
		}
		
		return $dependencies;
	}
	
	#########################
	#   PROJECT CHECKERS    #
	#########################
	
	public function isValidProject()
	{
		if (
			is_dir($this->root_path)
			&& is_dir($this->root_path . '/app') 
			&& file_exists($this->root_path . '/.env')
			&& file_exists($this->root_path . '/composer.json')
		) {
			
			return true;

		}
		
		return false;
		
	}
	
	
	
	public function isFile($path)
	{
		if (is_file($path)) return true;
		
		return false;
	}
	
	public function isDirectory($path)
	{
		
		if (is_dir($path)) return true;
		
		return false;
		
	}
	
	public function updateExists() // Phase this method out
	{
		
		if (is_dir($this->update_path)) return true;
		
		return false;
		
	}
	
	public function createUpdateDirectory()
	{
		if (mkdir($this->update_path)) return true;
			
		return false;
			
	}
	
	public function hasNodeModules()
	{
		if (!is_dir($this->root_path.'/node_modules')) return false;
		
		if (scandir($this->root_path.'/node_modules') <= 2) return false;
		
		if (!file_exists($this->root_path.'/package.json')) return false;
		
		return true;
	}
	
	public function hasGulp()
	{
		if (file_exists($this->root_path.'/gulpfile.js')) return true;
		
		return false;
		
	}
	
	public function hasGit()
	{
		
		if (file_exists($this->root_path.'/.git')) return true;
		
		return false;
		
	}
}
