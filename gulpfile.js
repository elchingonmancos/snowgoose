const elixir = require('laravel-elixir');

var asset_path = "./resources/assets/",
	bower_components_path = "./vendor/bower_components/";

elixir(function(mix) {
	mix.sass(['app.scss',bower_components_path + 'bootstrap-sass-official/assets/stylesheets',bower_components_path + 'font-awesome/sass'])
	.copy(bower_components_path + 'font-awesome/fonts/**', 'public/fonts')
	.copy(bower_components_path + 'bootstrap-sass-official/assets/fonts/bootstrap/**', 'public/fonts')
	.scripts([
		bower_components_path + 'jquery/dist/jquery.js',
		bower_components_path + 'jquery-ui/jquery-ui.js',
		bower_components_path + 'bootstrap-sass-official/assets/javascripts/bootstrap.js',
		asset_path + 'js/SnowGoose/SnowGoose.js',
		asset_path + 'js/SnowGoose/StatusUpdate.js',
		asset_path + 'js/SnowGoose/MigrationBuilder.js',
		asset_path + 'js/SnowGoose/HttpRunner.js',
		asset_path + 'js/dom_events/migration_builder.js',
		asset_path + 'js/dom_events/migration_runner.js',
		asset_path + 'js/dom_events/app.js',
	],'public/js/app.js','./')
	.version(['public/css/app.css','public/js/app.js']);
});