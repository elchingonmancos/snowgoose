var SnowGoose = (function(){
	
	var properties = {
		
		rootPath : "",
		
		projectTree : []
		
	}

	var setProp = function(name,value) {
		
		properties[name] = value;
		
	}

	var getProp = function(name) {
		
		return properties[name];
		
	}
	
	var setRootPath = function(path) {
		
		properties.rootPath = path;
		
		return true;
		
	}
	
	var getRootPath = function(path) {
		
		return properties.rootPath;
		
	}
	
	var setProjectTree = function(tree) {
		
		properties.projectTree = tree;
		
		return true;
		
	}
	
	var getProjectTree = function() {
		
		return properties.projectTree;
		
	}
	
	// Public Methods
	return {
		getProp : getProp,
		setProp : setProp,
		setRootPath : setRootPath,
		getRootPath : getRootPath,
		setProjectTree : setProjectTree,
		getProjectTree : getProjectTree
	}

})();