$(document).on('click','#project-submit-path',function(){
	
	var projectTargetPath = $('#project-target-path').val(),
		
		$projectProperties = $('#project'),
		
		$status = $('#project-path .status');
		
	$(".init-hide").hide();
	
	HttpRunner.isValidProject(projectTargetPath).then(function(response){
		
		console.log(response);
		
		response = JSON.parse(response);
	
		if (response.status == "success") {

			console.info("Valid project loaded.");
			
			SnowGoose.setRootPath(projectTargetPath);
			
			$('#migration-builder .builder').html(response.data.file_tree);
			
			$('#project-path .status')
			.addClass('success')
			.removeClass('failure')
			.text("Great! This a valid project path.")
			.fadeIn('slow', function(){
				
				$("#project-submit-path")
				.removeClass("btn-primary")
				.addClass("btn-default");
				
				$('#get-started')
				.fadeIn();
				
			});
				
			$("#project-properties .name .value")
			.text(response.data.dependencies.name);
			
			$("#project-properties .description .value")
			.text(response.data.dependencies.description);
			
			$("#project-properties .version .value")
			.text(response.data.dependencies.require['laravel/framework']);
			
			$("#project-properties .path .value")
			.text(projectTargetPath);
			
		} else {
			
			$('#project-path .status')
			.addClass('failure')
			.removeClass('success')
			.text("Bummer. This path isn't a valid project.")
			.fadeIn('slow');
			
		}
	});
});

$(document).on('click','#get-started button',function(){
	
	$('#project-path, #get-started').delay(500).fadeOut('slow',function(){
				
		$('#project-version, #project-properties, #version-select').fadeIn('slow'); // assign common class
		
	});
	
});

$(document).on('change','#project-version',function(){
	
	$(this).children('.install-project').addClass('btn-primary').removeClass('btn-default').prop('disabled',false);
	
});

$(document).on('click','.install-project',function(){
	
	console.info("SnowGoose is updating...");
	
	$this = $(this);
	
	var version = $('#project-version select').val();
	
	StatusUpdate.openModal("Install Laravel " + version);
	
	StatusUpdate.loaderOn();
	
	StatusUpdate.addUpdate("Downloading from packagist...");
	
	StatusUpdate.hideButtons();
	
	console.info("Downloading from packagist...");
	
	var path = SnowGoose.getRootPath();
	
	HttpRunner.installProjectUpdate(path,version).then(function(response){
		
		console.log(response);
		
		response = JSON.parse(response);
		
		StatusUpdate.loaderOff();
		
		StatusUpdate.showButtons();
		

		if (response.status === "failed") {
			
			StatusUpdate.addError(response.errors);
			
			return false;
			
		} else {

			
			console.info("Project downloaded!");
			
			StatusUpdate.showSuccess("Laravel Successfully downloaded!");
			
			$('.install-project').addClass('btn-default').removeClass('btn-primary').prop('disabled',true);
			
			$('.builder, #migration-builder, #run-migration').fadeIn('slow');
			
			$('#alt-migration-modal .builder').html(response.data.file_tree);

		}
		
	});
	
});