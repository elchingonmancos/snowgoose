<div id="project-version" class="form-group init-hide user-input">
				
	<label>Which version would you like to update to?</label>
	
	<select class="form-control">
	
		<option>Choose</option>
	
		@foreach($laravel_versions as $key => $version)
		
			@if(substr($key,0,1) == "v")
			
				<option value="{{ trim($key, 'v') }}">Laravel {{ $key }}</option>
			
			@endif
		
		@endforeach
	
	</select>
	
	<button class="btn btn-default install-project" disabled>Install</button>

</div>