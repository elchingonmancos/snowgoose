<?php

use Illuminate\Http\Request;

	Route::post('/init', 'SnowGooseController@init');
	
	Route::post('/get_project_tree', 'SnowGooseController@getProjectTree');
	
	Route::post('/install_update', 'SnowGooseController@installUpdate');
	
	Route::post('/migrate', 'SnowGooseController@migrate');
